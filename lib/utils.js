exports.$ = function $(el, sel=undefined) {
  if (typeof sel === 'string') {
    return el.querySelector(sel)
  }
  return document.querySelector(el)
}

exports.safe = function safe(str) {
  str = (str || '').toString()
  return str.replace(/</g, '&lt;').replace(/>/g, '&gt;')
}

exports.render = function render(html) {
  var template = document.createElement('template')
  template.innerHTML = html
  return template.content
}
