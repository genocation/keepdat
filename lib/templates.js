export function noteTextTemplate(note) {
  return `
    <div class="note ${note.color.toLowerCase()}">
      <h1>${note.title}</h1>
      <p>${note.textContent}</p>
    </div>
  `
}
