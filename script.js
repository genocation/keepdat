
class KeepDatApp {

  constructor() {
    this.notes = []
    this.noteContainer = $('#note-container')
    this.notePinned = $('#note-pinned')
    this.dat = new DatArchive(window.location)
    this.currentPage = 0
    this.pageSize = 20
    this.isoOptions = {
      itemSelector: '.note',
      layoutMode: 'masonry',
      masonry: {
        gutter: 10,
        columnWidth: 20
      }
    }
    // Initialize isotope grid for notes
    this.board = new Isotope(this.noteContainer, this.isoOptions)
    // Initialize isotope grid for notes
    this.pins = new Isotope(this.notePinned, this.isoOptions)
  }

  async load() {
    console.log('loading notes...')

    // Instantiate the site archive
    const noteFiles = await this.dat.readdir('/notes')

    // Store the notes file names
    noteFiles.forEach(async filename => {
      if (filename.split('.').pop() === 'json') {
        var url = window.location + 'notes/' + filename
        this.notes.push(filename)
      }
    })

    // Sort and reverse, we want last notes on top
    this.notes.sort().reverse()
    console.log('loaded!')

    this.loadPage(this.currentPage)
  }

  loadNext() {
    this.currentPage += 1
    this.loadPage(this.currentPage)
  }

  async loadPage(n) {
    const page = this.notes.slice(n*this.pageSize, n*this.pageSize + this.pageSize)
    page.forEach((filename) => {
      this.loadNote(filename)
    })
  }

  loadTextNote(data) {
    const note = render(noteTextTemplate(data))
    this.noteContainer.append(note)
    this.board.appended(note)
  }

  loadListNote(data) {
    const note = render(noteListTemplate(data))
    this.noteContainer.append(note)
    this.board.appended(note)
  }

  async loadNote(filename) {
    var buf = await this.dat.readFile('notes/' + filename, 'utf8').then(content => {
      try {
        const data = JSON.parse(content)
        data.date = filenameToDate(filename)
        if (data.hasOwnProperty('textContent')) {
          this.loadTextNote(data)
        } else if (data.hasOwnProperty('listContent')) {
          this.loadListNote(data)
        }
      } catch(e) {
        console.log(e)
        console.log(content)
      }
    })
  }
}

function noteTextTemplate(note) {
  const archived = note.isArchived ? 'archived' : ''
  return `
    <div class="note ${note.color} ${archived}">
      <h2>${note.title}</h2>
      <p>${reformat(note.textContent)}</p>
      <small>${note.date}</small>
    </div>
  `
}

function noteListTemplate(note) {
  const archived = note.isArchived ? 'archived' : ''
  let list = ''
  note.listContent.forEach((c, index) => {
    let checked = c.isChecked ? 'checked' : ''
    list += `
    <li>
      <input type="checkbox" id="item${index}" name="item${index}" value="${index}" ${checked}>
      <label for="item${index}">${reformat(c.text)}</label><br>
    </li>
    `
  })
  return `
    <div class="note ${note.color} ${archived}">
      <h2>${note.title}</h2>
      <ul>${list}</ul>
      <small>${note.date}</small>
    </div>
  `
}

function filenameToDate(filename) {
  let filearr = filename.split('.')
  filearr.pop()
  const datestr = filearr.join('.').split('_').join(':')
  return moment(datestr).format('MMMM Do YYYY, h:mm:ss')
}

function $(el, sel=undefined) {
  if (typeof sel === 'string') {
    return el.querySelector(sel)
  }
  return document.querySelector(el)
}

function safe(str) {
  str = (str || '').toString()
  return str.replace(/</g, '&lt;').replace(/>/g, '&gt;')
}

function eol(str) {
  return str.replace(/(?:\r\n|\r|\n)/g, '<br/>')
}

function render(html) {
  var template = document.createElement('div')
  template.innerHTML = html.trim()
  return template.firstChild
}

function urlify(text) {
  var urlRegex = /(https?:\/\/[^\s]+)/g
  return text.replace(urlRegex, (url) => {
    return '<a href="' + url + '" class="innerlink" target="_blank">' + url + '</a>'
  })
}

function reformat(text) {
  return urlify(eol(safe(text)))
}

document.addEventListener('DOMContentLoaded', () => {
  window.app = new KeepDatApp()
  window.app.load()
})

document.getElementById("show-more").addEventListener('click', () => {
  window.app.loadNext()
})

document.getElementById("add-note").addEventListener('click', () => {

})
